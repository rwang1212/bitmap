"""
ipythonblocks provides a BlockGrid class that displays a colored grid in the
IPython Notebook. The colors can be manipulated, making it useful for
practicing control flow stuctures and quickly seeing the results.

"""

# This file is copyright 2013 by Matt Davis and covered by the license at
# https://github.com/jiffyclub/ipythonblocks/blob/master/LICENSE.txt

import copy
import collections
import json
import numbers
import os
import sys
import time
import uuid

from operator import iadd
from functools import reduce

from IPython.display import HTML, display, clear_output
from IPython.display import Image as ipyImage
from PIL import Image, ImageFont, ImageDraw, ImageEnhance

__all__ = ('Block', 'BlockGrid', 'Pixel', 'ImageGrid',
           'InvalidColorSpec', 'ShapeMismatch', 'show_color',
           'embed_colorpicker', 'colors', 'fui_colors', '__version__')
__version__ = '1.7.0'

_TABLE = ('<style type="text/css">'
          'table.blockgrid {{border: none;}}'
          ' .blockgrid tr {{border: none;}}'
          ' .blockgrid td {{padding: 0px;}}'
          ' #blocks{0} td {{border: {1}px solid white;}}'
          '</style>'
          '<table id="blocks{0}" class="blockgrid"><tbody>{2}</tbody></table>')
_TR = '<tr>{0}</tr>'
_TD = ('<td title="{0}" style="width: {1}px; height: {2}px;'
       'background-color: {3};" id="{4}"></td>')
_TD2 = ('<td title="{0}" class="{1}" style="width: {2}px; height: {3}px;'
       'background-color: {4}; text-align: center" id="{5}">{6}</td>')
_RGB = 'rgb({0}, {1}, {2})'
_TITLE = 'Index: [{0}, {1}]&#10;Color: ({2}, {3}, {4})'
_TITLE2 = 'Die: [{0}, {1}]&#10;Value: {2}'

_SINGLE_ITEM = 'single item'
_SINGLE_ROW = 'single row'
_ROW_SLICE = 'row slice'
_DOUBLE_SLICE = 'double slice'

_SMALLEST_BLOCK = 1

def _flatten(thing, ignore_types=(str,)):
    """
    Yield a single item or str/unicode or recursively yield from iterables.

    Adapted from Beazley's Python Cookbook.

    """
    if isinstance(thing, collections.Iterable) and \
            not isinstance(thing, ignore_types):
        for i in thing:
            for x in _flatten(i):
                yield x
    else:
        yield thing
        
def _color_property(name):
    real_name = "_" + name

    @property
    def prop(self):
        return getattr(self, real_name)

    @prop.setter
    def prop(self, value):
        value = Block._check_value(value)
        setattr(self, real_name, value)

    return prop

class Block(object):
    """
    A colored square.

    Parameters
    ----------
    red, green, blue : int
        Integers on the range [0 - 255].
    size : int, optional
        Length of the sides of this block in pixels. One is the lower limit.

    Attributes
    ----------
    red, green, blue : int
        The color values for this `Block`. The color of the `Block` can be
        updated by assigning new values to these attributes.
    rgb : tuple of int
        Tuple of (red, green, blue) values. Can be used to set all the colors
        at once.
    row, col : int
        The zero-based grid position of this `Block`.
    size : int
        Length of the sides of this block in pixels. The block size can be
        changed by modifying this attribute. Note that one is the lower limit.

    """

    red = _color_property('red')
    green = _color_property('green')
    blue = _color_property('blue')

    def __init__(self, red, green, blue,tooltip=None, size=20, aspect_ratio=0.5):
        self.red = red
        self.green = green
        self.blue = blue
        self.size = size
        self.aspect_ratio = aspect_ratio
        self.tooltip = tooltip

        self._row = None
        self._col = None

    @staticmethod
    def _check_value(value):
        """
        Check that a value is a number and constrain it to [0 - 255].

        """
        if not isinstance(value, numbers.Number):
            s = 'value must be a number. got {0}.'.format(value)
            raise InvalidColorSpec(s)

        return int(round(min(255, max(0, value))))

    @property
    def rgb(self):
        return (self._red, self._green, self._blue)

    @rgb.setter
    def rgb(self, colors):
        if len(colors) != 3:
            s = 'Setting colors requires three values: (red, green, blue).'
            raise ValueError(s)

        self.red, self.green, self.blue = colors

    @property
    def row(self):
        return self._row

    @property
    def col(self):
        return self._col

    @property
    def size(self):
        return self._size

    @size.setter
    def size(self, size):
        self._size = max(_SMALLEST_BLOCK, size)

    def set_colors(self, red, green, blue):
        """
        Updated block colors.

        Parameters
        ----------
        red, green, blue : int
            Integers on the range [0 - 255].

        """
        self.red = red
        self.green = green
        self.blue = blue

    def _update(self, other):
        if isinstance(other, Block):
            self.rgb = other.rgb
            self.size = other.size
        elif isinstance(other, collections.Sequence) and len(other) == 2:
           self.rgb = other[0]
           self.tooltip = other[1]
        elif isinstance(other, collections.Sequence) and len(other) == 3:
           self.rgb = other
           self.tooltip = None
        else:
            errmsg = (
                'Value must be a Block or a sequence of 3 integers. '
                'Got {0!r}.'
            )
            raise ValueError(errmsg.format(other))

    @property
    def _td(self):
        """
        The HTML for a table cell with the background color of this Block.

        """
        rgb = _RGB.format(self._red, self._green, self._blue)
        if self.tooltip == None:
          title = _TITLE.format(self._row, self._col,
                              self._red, self._green, self._blue)
          idval = '{},{}'.format(self._row,self._col)
          return _TD.format(title, self._size, self._size * self.aspect_ratio, rgb,idval)
        else:
          if len(self.tooltip) == 4:          
            #Format of tooltip
            #('title','class','id','text')
            title = self.tooltip[0]
            classtxt = self.tooltip[1]
            idtxt = self.tooltip[2]
            text = self.tooltip[3]
            #print "{}\n{}\n{}\n{}\n".format(title,classtxt,idtxt,text)
            return _TD2.format(title, classtxt, self._size, self._size * self.aspect_ratio, rgb, idtxt,text)
          else:  
            rgb = _RGB.format(self._red, self._green, self._blue)
            title = _TITLE.format(self._row, self._col,
                                self._red, self._green, self._blue)
            idval = '{},{}'.format(self._row,self._col)
            return _TD.format(title, self._size, self._size * self.aspect_ratio, rgb,idval)
        
    def tooltip_text(self):
        if len(self.tooltip) == 4:          
            #Format of tooltip
            #('title','class','id','text')
            title = self.tooltip[0]
            classtxt = self.tooltip[1]
            idtxt = self.tooltip[2]
            text = self.tooltip[3]
            #print "{}\n{}\n{}\n{}\n".format(title,classtxt,idtxt,text)
            return text


    def _repr_html_(self):
        return _TABLE.format(uuid.uuid4(), 0, _TR.format(self._td))

    def show(self):
        display(HTML(self._repr_html_()))

    __hash__ = None

    def __eq__(self, other):
        if not isinstance(other, Block):
            return False
        return self.rgb == other.rgb and self.size == other.size

    def __str__(self):
        s = ['{0}'.format(self.__class__.__name__),
             'Color: ({0}, {1}, {2})'.format(self._red,
                                             self._green,
                                             self._blue)]

        # add position information if we have it
        if self._row is not None:
            s[0] += ' [{0}, {1}]'.format(self._row, self._col)

        return os.linesep.join(s)

    def __repr__(self):
        type_name = type(self).__name__
        return '{0}({1}, {2}, {3}, size={4})'.format(type_name,
                                                     self.red,
                                                     self.green,
                                                     self.blue,
                                                     self.size)

class BlockGrid(object):
    """
    A grid of blocks whose colors can be individually controlled.

    Parameters
    ----------
    width : int
        Number of blocks wide to make the grid.
    height : int
        Number of blocks high to make the grid.
    fill : tuple of int, optional
        An optional initial color for the grid, defaults to black.
        Specified as a tuple of (red, green, blue). E.g.: (10, 234, 198)
    block_size : int, optional
        Length of the sides of grid blocks in pixels. One is the lower limit.
    lines_on : bool, optional
        Whether or not to display lines between blocks.

    Attributes
    ----------
    width : int
        Number of blocks along the width of the grid.
    height : int
        Number of blocks along the height of the grid.
    shape : tuple of int
        A tuple of (width, height).
    block_size : int
        Length of the sides of grid blocks in pixels. The block size can be
        changed by modifying this attribute. Note that one is the lower limit.
    lines_on : bool
        Whether lines are shown between blocks when the grid is displayed.
        This attribute can used to toggle the whether the lines appear.

    """

    def __init__(self, width, height, fill=(0, 0, 0),
                 block_size=20, aspect_ratio=0.5, lines_on=True, tooltip=None):
        self._width = width
        self._height = height
        self._block_size = block_size
        self.aspect_ratio = aspect_ratio
        self.lines_on = lines_on
        self._tooltip = tooltip
        self._initialize_grid(fill,tooltip)

    def _initialize_grid(self, fill, tooltip):
        grid = [[Block(*fill, tooltip=self._tooltip, size=self._block_size, aspect_ratio=self.aspect_ratio)
                for col in range(self.width)]
                for row in range(self.height)]

        self._grid = grid

    @property
    def width(self):
        return self._width

    @property
    def height(self):
        return self._height

    @property
    def shape(self):
        return (self._width, self._height)

    @property
    def block_size(self):
        return self._block_size

    @block_size.setter
    def block_size(self, size):
        self._block_size = size

        for block in self:
            block.size = size

    @property
    def lines_on(self):
        return self._lines_on

    @lines_on.setter
    def lines_on(self, value):
        if value not in (0, 1):
            s = 'lines_on may only be True or False.'
            raise ValueError(s)

        self._lines_on = value

    def __eq__(self, other):
        if not isinstance(other, BlockGrid):
            return False
        else:
            # compare the underlying grids
            return self._grid == other._grid

    def _view_from_grid(self, grid):
        """
        Make a new grid from a list of lists of Block objects.

        """
        new_width = len(grid[0])
        new_height = len(grid)

        new_BG = self.__class__(new_width, new_height,
                                block_size=self._block_size,
                                lines_on=self._lines_on)
        new_BG._grid = grid

        return new_BG

    @staticmethod
    def _categorize_index(index):
        """
        Used by __getitem__ and __setitem__ to determine whether the user
        is asking for a single item, single row, or some kind of slice.

        """
        if isinstance(index, int):
            return _SINGLE_ROW

        elif isinstance(index, slice):
            return _ROW_SLICE

        elif isinstance(index, tuple):
            if len(index) > 2:
                s = 'Invalid index, too many dimensions.'
                raise IndexError(s)

            elif len(index) == 1:
                s = 'Single indices must be integers, not tuple.'
                raise TypeError(s)

            if isinstance(index[0], slice):
                if isinstance(index[1], (int, slice)):
                    return _DOUBLE_SLICE

            if isinstance(index[1], slice):
                if isinstance(index[0], (int, slice)):
                    return _DOUBLE_SLICE

            elif isinstance(index[0], int) and isinstance(index[0], int):
                return _SINGLE_ITEM

        raise IndexError('Invalid index.')

    def __getitem__(self, index):
        ind_cat = self._categorize_index(index)

        if ind_cat == _SINGLE_ROW:
            return self._view_from_grid([self._grid[index]])

        elif ind_cat == _SINGLE_ITEM:
            block = self._grid[index[0]][index[1]]
            block._row, block._col = index
            return block

        elif ind_cat == _ROW_SLICE:
            return self._view_from_grid(self._grid[index])

        elif ind_cat == _DOUBLE_SLICE:
            new_grid = self._get_double_slice(index)
            return self._view_from_grid(new_grid)

    def __setitem__(self, index, value):
        thing = self[index]

        if isinstance(value, BlockGrid):
            if isinstance(thing, BlockGrid):
                if thing.shape != value.shape:
                    raise ShapeMismatch('Both sides of grid assignment must '
                                        'have the same shape.')
                for a, b in zip(thing, value):
                    a._update(b)

            else:
                raise TypeError('Cannot assign grid to single block.')

        elif isinstance(value, (collections.Iterable, Block)):
            for b in _flatten(thing):
                b._update(value)

    def _get_double_slice(self, index):
        sl_height, sl_width = index

        if isinstance(sl_width, int):
            if sl_width == -1:
                sl_width = slice(sl_width, None)
            else:
                sl_width = slice(sl_width, sl_width + 1)

        if isinstance(sl_height, int):
            if sl_height == -1:
                sl_height = slice(sl_height, None)
            else:
                sl_height = slice(sl_height, sl_height + 1)

        rows = self._grid[sl_height]
        grid = [r[sl_width] for r in rows]

        return grid

    def __iter__(self):
        for r in range(self.height):
            for c in range(self.width):
                yield self[r, c]

    def animate(self, stop_time=0.2):
        """
        Call this method in a loop definition to have your changes to the grid
        animated in the IPython Notebook.

        Parameters
        ----------
        stop_time : float
            Amount of time to pause between loop steps.

        """
        for block in self:
            self.show()
            time.sleep(stop_time)
            yield block
            clear_output()
        self.show()

    def _repr_html_(self):
        rows = range(self._height)
        cols = range(self._width)

        html = reduce(iadd,
                      (_TR.format(reduce(iadd,
                                         (self[r, c]._td
                                          for c in cols)))
                       for r in rows))

        return _TABLE.format(uuid.uuid4(), int(self._lines_on), html)

    def __str__(self):
        s = ['{0}'.format(self.__class__.__name__),
             'Shape: {0}'.format(self.shape)]

        return os.linesep.join(s)

    def copy(self):
        """
        Returns an independent copy of this BlockGrid.

        """
        return copy.deepcopy(self)

    def show(self):
        """
        Display colored grid as an HTML table.

        """
        display(HTML(self._repr_html_()))

    def flash(self, display_time=0.2):
        """
        Display the grid for a time.

        Useful for making an animation or iteratively displaying changes.

        Parameters
        ----------
        display_time : float
            Amount of time, in seconds, to display the grid.

        """
        self.show()
        time.sleep(display_time)
        clear_output()

    def _calc_image_sizeee(self):
        """
        Calculate the size, in pixels, of the grid as an image.

        Returns
        -------
        px_width : int
        px_height : int

        """
        px_width = self._block_size * self._width
        px_height = self._block_size * self._height

        if self._lines_on:
            px_width += self._width + 1
            px_height += self._height + 1

        return px_width, px_height
    
    def _calc_image_size(self):
        """
        Calculate the size, in pixels, of the grid as an image.

        Returns
        -------
        px_width : int
        px_height : int

        """
        px_width = self._block_size * self._width
        px_height = int(round(self._block_size * self.aspect_ratio * self._height))
#        print 'aspect_ratio isisis', self.aspect_ratio, px_width, px_height, self._block_size

        if self._lines_on:
            px_width += self._width + 1
            px_height += self._height + 1

        return px_width, px_height

    def _write_image(self, fp, format='png'):
        """
        Write an image of the current grid to a file-object.

        Parameters
        ----------
        fp : file-like
            A file-like object such as an open file pointer or
            a StringIO/BytesIO instance.
        format : str, optional
            An image format that will be understood by PIL,
            e.g. 'png', 'jpg', 'gif', etc.

        """
        try:
            # PIL
            import Image
            import ImageDraw
        except ImportError:
            # pillow
            from PIL import Image, ImageDraw

        im = Image.new(
            mode='RGB', size=self._calc_image_size(), color=(255, 255, 255))
        draw = ImageDraw.Draw(im)

        _bs = self._block_size

        for r in range(self._height):
            for c in range(self._width):
                px_r = r * _bs * self.aspect_ratio
                px_c = c * _bs
                if self._lines_on:
                    px_r += r + 1
                    px_c += c + 1

                rect = ((px_c, px_r), (px_c + _bs - 1, px_r + _bs * self.aspect_ratio - 1))
                draw.rectangle(rect, fill=self._grid[r][c].rgb)
                draw.text((px_c+5, px_r+7), Block.tooltip_text(self._grid[r][c]), font=ImageFont.truetype("arial",14), fill='black')

        im.save(fp, format=format)

    def show_image(self):
        """
        Embed grid in the notebook as a PNG image.

        """
        if sys.version_info[0] == 2:
            from StringIO import StringIO as BytesIO
        elif sys.version_info[0] == 3:
            from io import BytesIO

        im = BytesIO()
        self._write_image(im)
        display(ipyImage(data=im.getvalue(), format='png'))

    def save_image(self, filename):
        """
        Save an image representation of the grid to a file.
        Image format will be inferred from file extension.

        Parameters
        ----------
        filename : str
            Name of file to save to.

        """
        with open(filename, 'wb') as f:
            self._write_image(f, format=filename.split('.')[-1])

    def to_text(self, filename=None):
        """
        Write a text file containing the size and block color information
        for this grid.

        If no file name is given the text is sent to stdout.

        Parameters
        ----------
        filename : str, optional
            File into which data will be written. Will be overwritten if
            it already exists.

        """
        if filename:
            f = open(filename, 'w')
        else:
            f = sys.stdout

        s = ['# width height', '{0} {1}'.format(self.width, self.height),
             '# block size', '{0}'.format(self.block_size),
             '# initial color', '0 0 0',
             '# row column red green blue']
        f.write(os.linesep.join(s) + os.linesep)

        for block in self:
            things = [str(x) for x in (block.row, block.col) + block.rgb]
            f.write(' '.join(things) + os.linesep)

        if filename:
            f.close()

    def _to_simple_grid(self):
        """
        Make a simple representation of the table: nested lists of
        of the rows containing tuples of (red, green, blue, size)
        for each of the blocks.

        Returns
        -------
        grid : list of lists
            No matter the class this method is called on the returned
            grid will be Python-style: row oriented with the top-left
            block in the [0][0] position.

        """
        return [[(x.red, x.green, x.blue, x.size) for x in row]
                for row in self._grid]

    def _construct_post_request(self, code_cells, secret):
        """
        Construct the request dictionary that will be posted
        to ipythonblocks.org.

        Parameters
        ----------
        code_cells : int, str, slice, or None
            Specify any code cells to be sent and displayed with the grid.
            You can specify a single cell, a Python, slice, or a combination
            as a string separated by commas.

            For example, '3,5,8:10' would send cells 3, 5, 8, and 9.
        secret : bool
            If True, this grid will not be shown randomly on ipythonblocks.org.

        Returns
        -------
        request : dict

        """
        if code_cells is not None:
            code_cells = _get_code_cells(code_cells)

        req = {
            'python_version': tuple(sys.version_info),
            'ipb_version': __version__,
            'ipb_class': self.__class__.__name__,
            'code_cells': code_cells,
            'secret': secret,
            'grid_data': {
                'lines_on': self.lines_on,
                'width': self.width,
                'height': self.height,
                'blocks': self._to_simple_grid()
            }
        }

        return req

    def post_to_web(self, code_cells=None, secret=False):
        """
        Post this grid to ipythonblocks.org and return a URL to
        view the grid on the web.

        Parameters
        ----------
        code_cells : int, str, or slice, optional
            Specify any code cells to be sent and displayed with the grid.
            You can specify a single cell, a Python, slice, or a combination
            as a string separated by commas.

            For example, '3,5,8:10' would send cells 3, 5, 8, and 9.
        secret : bool, optional
            If True, this grid will not be shown randomly on ipythonblocks.org.

        Returns
        -------
        url : str
            URL to view your grid on ipythonblocks.org.

        """
        import requests

        req = self._construct_post_request(code_cells, secret)
        response = requests.post(_POST_URL, data=json.dumps(req))
        response.raise_for_status()

        return response.json()['url']

    def _load_simple_grid(self, block_data):
        """
        Modify the grid to reflect the data in `block_data`, which
        should be a nested list of tuples as produced by `_to_simple_grid`.

        Parameters
        ----------
        block_data : list of lists
            Nested list of tuples as produced by `_to_simple_grid`.

        """
        if len(block_data) != self.height or \
                len(block_data[0]) != self.width:
            raise ShapeMismatch('block_data must have same shape as grid.')

        for row in range(self.height):
            for col in range(self.width):
                self._grid[row][col].rgb = block_data[row][col][:3]
                self._grid[row][col].size = block_data[row][col][3]

    @classmethod
    def from_web(cls, grid_id, secret=False):
        """
        Make a new BlockGrid from a grid on ipythonblocks.org.

        Parameters
        ----------
        grid_id : str
            ID of a grid on ipythonblocks.org. This will be the part of the
            URL after 'ipythonblocks.org/'.
        secret : bool, optional
            Whether or not the grid on ipythonblocks.org is secret.

        Returns
        -------
        grid : BlockGrid

        """
        import requests

        get_url = _GET_URL_PUBLIC if not secret else _GET_URL_SECRET
        resp = requests.get(get_url.format(grid_id))
        resp.raise_for_status()
        grid_spec = resp.json()

        grid = cls(grid_spec['width'], grid_spec['height'],
                   lines_on=grid_spec['lines_on'])
        grid._load_simple_grid(grid_spec['blocks'])

        return grid
		

from IPython.display import display,clear_output,Javascript,HTML
import random
import numpy as np;
import math
import PIL 
from PIL import Image, ImageFont, ImageDraw, ImageEnhance
import matplotlib.colors as pltcolor

class WaferBitMap(object):   
  """
    Parameters
    ----------
    df_DrawMap: dataframe
        DataFrame with row, col, plot columns
    plot: str or list
        The names of the DataFrame column that contains the data/values want to deploy on the wafer map
    x: str, optional 
        The name of the DataFrame column that contains physical coordinations of x, e.g., PLOTROW
    y: str, optional 
        The name of the DataFrame column that contains physical coordinations of y, e.g., PLOTCOL
    rdir: str, optional 
        Row direction <Down/Up>
    cdir: str, optional
        Column direction <Right/Left>
    blocksize: int, optional 
        Pixel size of block length
    aspect_ratio: float, optional
        Ratio of block width to length
    colors: list, optional
        RGB color range to be used for grading, e.g., [(0, 255, 0),(255,255,0),(255, 0, 0)]
    disptext: str, optional
        Switch to show value on the wafer map <True/False>
    axis: str, optional
        Display axis label on WaferMap <True/False>
    saveimage: str, optional
        Name and path to save the wafer map image, e.g., Single Bit.png
    Noshow: str, optional
        Switch to show the wafer map <True/False>

    Returns
    ----------
    Wafer map visualization with the input data and scale bar;
    optional wafermap image saving:  <wafermap>.png
    optional scalebar saving:  <scalebar>.png

    Example
    ----------
    This shows the default syntax
    WaferMap(df_DrawMap=None, x='PLOTROW', y='PLOTCOL', rdir='Up', cdir='Right', blocksize=20, aspect_ratio=0.5, colors=[(0, 255, 0), (255, 255, 0), (255, 0, 0)], disptext=False, saveimage=None, axis=False, noshow=False)
  """   

  def __init__(self, df_DrawMap=None, plot=None, x='PLOTROW', y='PLOTCOL', rdir='Up', cdir='Right', blocksize=45, aspect_ratio=0.5, colors =[(0, 255, 0),(255,255,0),(255, 0, 0)], disptext=True, saveimage=None, axis=True, noshow=False):  

      #Reticle view
      self.df_DrawMap = df_DrawMap
      self.row = x
      self.col = y
      self.plot = plot
      self.rdir = rdir
      self.cdir = cdir
      self.blocksize = blocksize
      self.colors = colors
      self.disptext = disptext
      self.saveimage = saveimage
      self.axis = axis
      self.noshow = noshow
      style_HTML = """<style type="text/css">
         table {border: none;} 
         tr {border: none;} 
         td {padding: 0px;} 
         td.retTL  { border-top: 3px solid black !important; border-left: 3px solid black !important}
         td.retT   { border-top: 3px solid black !important; }
         td.retTR  { border-top: 3px solid black !important; border-right: 3px solid black !important}
         td.retL   { border-left: 3px solid black !important; }
         td.retR   { border-right: 3px solid black !important; }
         td.retBL  { border-bottom: 3px solid black !important; border-left: 3px solid black !important}
         td.retBR  { border-bottom: 3px solid black !important; border-right: 3px solid black !important}
         td.retB   { border-bottom: 3px solid black !important; }
         td.retBTL  { border-bottom: 3px solid black !important; border-top: 3px solid black !important; border-left: 3px solid black !important}
         td.retBTR  { border-bottom: 3px solid black !important; border-top: 3px solid black !important; border-right: 3px solid black !important}
         td.retTLR  { border-top: 3px solid black !important; border-left: 3px solid black !important; border-right: 3px solid black !important}
         td.retBLR  { border-bottom: 3px solid black !important; border-left: 3px solid black !important; border-right: 3px solid black !important}
         td.retBTLR  { border-bottom: 3px solid black !important; border-top: 3px solid black !important; border-left: 3px solid black !important; border-right: 3px solid black !important}
         td.mapcell { width: 30px; height: 20px; cursor: pointer; }
         </style>
      """
      display(HTML(style_HTML))
      self.WaferMap()

  def retmap(self,x,y,x_modulus,y_modulus,x_floor,y_floor):
      if y_modulus == 0:
          if y < y_floor:
              style_text = 'retB'
          elif y > y_floor:
              style_text = 'retT'
          else:
              style_text = 'retBT'
      else:
          if y < y_floor:
              style_text = 'retB'
          elif y > y_floor:
              style_text = 'retT'
          else:
              style_text = 'retBT'  
      if x_modulus == 0:
          if x < x_floor:
              style_text += 'R'
          elif x > x_floor:
              style_text += 'L'
          else:
              style_text += 'LR'
      else:
          if x < x_floor:
              style_text += 'R'
          elif x > x_floor:
              style_text += 'L'
          else:
              style_text += 'LR'
      return style_text

  def rgb_to_hex(self,rgb):
#      return '#%02x%02x%02x' % rgb
      hex = '#%02x%02x%02x' % rgb
      return hex
  
  def hex_to_rgb(self,value):
      rgb = ()
      value = value.lstrip('#')
      lv = len(value)
      rgb = tuple(int(value[i:i + lv // 3], 16) for i in range(0, lv, lv // 3))
      return rgb
    
  #Heatmap color generator
  def convert_to_rgb(self,minval, maxval, val, colors):
      """Generates gradient RGB values for BlockGrid application
         minval => minimum value in the list
         maxval => maximum value in the list
         val    => Value used to calculate RGB code
         colors => List of RGBs to be used
     """
      ColorValue = ()
      max_index = len(colors)-1
      if maxval == minval:
          v, i1, i2 = 0, 0, 0
      else:
          v = float(val-minval) / float(maxval-minval) * max_index
          i1, i2 = int(v), min(int(v)+1, max_index)
      (r1, g1, b1), (r2, g2, b2) = colors[i1], colors[i2]
      f = v - i1
      ColorValue = int(r1 + f*(r2-r1)), int(g1 + f*(g2-g1)), int(b1 + f*(b2-b1))
      return ColorValue
  
  def colorBar(self,colors,maxval):
      """Creates Blockgrid based on the input color list"""
      try:
          # PIL
          import Image
          import ImageDraw
      except ImportError:
          # pillow
          from PIL import Image, ImageDraw
      FineColors = []
      for i in range(len(colors)):          
          if i<(len(colors)-1):
              r,g,b = [],[],[]
              (r1, g1, b1), (r2, g2, b2) = colors[i], colors[i+1]
              if r1==r2:
                  for deltar in range(0,255//5):
                      r.append(r1)
              else:
                  for deltar in range(r1,r2,(r2-r1)//51):
                      r.append(deltar)
              if g1==g2:
                  for deltag in range(0,255//5):
                      g.append(g1)
              else:
                  for deltag in range(g1,g2,(g2-g1)//51):
                      g.append(deltag)
              if b1==b2:
                  for deltab in range(0,255//5):
                      b.append(b1)
              else:
                  for deltab in range(b1,b2,(b2-b1)//51):
                      b.append(deltab)
              for j in range(0,255//5):
                  FineColors.append((r[j],g[j],b[j]))
      FineColors.append((255, 0, 0))
      Colours = {}
      for i,val in enumerate(FineColors):
          Colours[i] = FineColors[i]
      return FineColors, Colours

  def html_table(self,d,maxval):
      keys = d.keys()
      items = ['<html>\n<head>\n','<style>','table, th, td {border: none !important;} th, td {padding: 0px; vertical-align:bottom !important; text-align:left !impoortant} </style>\n</head>\n','<body>\n<table style="width:400px; height:60px">', '<tr style="display:inline !important;color:black;background-color:#ffffff;font-size:12px;text-align:left">']
      items.append('<td>0</td>\n<td><pre>                                                      {}</pre></td>\n</tr>\n<tr style="color:white">'.format(int(maxval)))
      for k in keys:
          items.append('<td bgcolor={}></td>'.format(self.rgb_to_hex(d[k])))
      items.append('</tr>')
      items.append('</table>')
      items.append('\n<body>\n</html>')
      ScaleBar_html = '\n'.join(items)
      return ScaleBar_html
        
  def ScaleBar_display(self,ScaleBar_html):
      if self.noshow == False:
          display(HTML(ScaleBar_html))

  def ScaleBar_save(self, FineColors, block_size, aspect_ratio, maxval):
      scale_blck = {}
      FineColors.reverse()

      for i,val in enumerate(FineColors):
          scale_blck[i,0] = ((255,255,255))
          scale_blck[i,1] = (FineColors[i])
          scale_blck[i,2] = ((255,255,255))
    
      Bar_height = len(FineColors)
      im = Image.new(mode='RGB', size=(block_size*3,int(round(block_size*Bar_height*aspect_ratio))), color=(255, 255, 255))
      draw = ImageDraw.Draw(im)

      for r in range(len(FineColors)):
            for c in range(2):
                px_r = r * block_size * aspect_ratio 
                px_c = c * block_size
                rect = ((px_c, px_r), (px_c + block_size - 1, px_r + block_size * aspect_ratio - 1))
                draw.rectangle(rect, fill=scale_blck[r,c])

      for r in range(10):
          px_r = r * block_size * 0.5 
          px_c = 2 * block_size
          rect = ((px_c, px_r), (px_c + block_size - 1, px_r + block_size * 0.5 - 1))
          draw.rectangle(rect, fill=((255,255,255)))
          if r==9:
              draw.text((px_c, px_r+19), '0', font=ImageFont.truetype("arial",14), fill='black')
          if r==0:
              draw.text((px_c, px_r), str(int(maxval)), font=ImageFont.truetype("arial",14), fill='black')

      im.save('scale.png', format='png')  

  def WaferMap (self):
      """WaferMap function to draw HeatMap/WaferMap
      """   

      xlist = self.df_DrawMap[self.row].unique()
      ylist = self.df_DrawMap[self.col].unique()

      if self.rdir == 'Down':
          xlist = sorted(xlist,reverse=False)
      else:
          xlist = sorted(xlist,reverse=True)
            
      if self.cdir == 'Right':
          ylist = sorted(ylist,reverse=False)
      else:
          ylist = sorted(ylist,reverse=True)
            
      self.grid = BlockGrid(len(ylist)+1,len(xlist)+1,aspect_ratio = 1. * len(ylist)/len(xlist),block_size = self.blocksize,fill=(255,255,255))

      if self.axis == True:
          for i in range(len(xlist)):
              title = ''
              classtxt = ''
              idtxt = ''
              dx = xlist[i]
#              print 'dx is ',dx
              temp = self.df_DrawMap[(self.df_DrawMap[self.row] == dx)]
#              print 'temp is\n', temp
              if len(temp['DIEY'].unique()) == 1:
                  text = '   ['+str(temp['DIEY'].unique()[0])+']'
                  axisxtxt = 'Y'
              else:
                  text = '   ['+str(temp['DIEX'].unique()[0])+']'
                  axisxtxt = 'X'
#              print 'text is',text, 'axistxt is', axisxtxt
              self.grid[i+1,0] = ((255,255,255),(title,classtxt,idtxt,text))

          for j in range(len(ylist)):
              title = ''
              classtxt = ''
              idtxt = ''
              dy = ylist[j]
              temp = self.df_DrawMap[(self.df_DrawMap[self.col] == dy)]
              if len(temp['DIEX'].unique()) == 1:
                  text = '   ['+str(temp['DIEX'].unique()[0])+']'
                  axisytxt = 'X'
              else:
                  text = '   ['+str(temp['DIEY'].unique()[0])+']'
                  axisytxt = 'Y'
              self.grid[0,j+1] = ((255,255,255),(title,classtxt,idtxt,text))
          title = ''
          classtxt = ''
          idtxt = ''
          axistxt = '{},{}'.format(axisxtxt,axisytxt) 
          self.grid[0,0] = ((255,255,255),(title,classtxt,idtxt,axistxt))
          minval = 0
          maxval = self.df_DrawMap[self.plot].max()
        
          for i in range(len(xlist)):
            for j in range(len(ylist)):
              dx = xlist[i]
              dy = ylist[j]
              posx = i + 1
              posy = j + 1
              temp = self.df_DrawMap[(self.df_DrawMap[self.row] == dx) & (self.df_DrawMap[self.col] == dy)]
              text = ''
              title =''
              prntx=dx
              prnty=dy
              if temp.empty:
                  classtxt ="" 
              else:
                  val = temp[self.plot].unique()[0]
                  prntx = temp['DIEX'].unique()[0]
                  prnty = temp['DIEY'].unique()[0]
                  classtxt = temp['IGNR_CLASS'].unique()[0]
              idtxt = '{},{}'.format(prntx,prnty)
              if temp.empty == True:
                  self.grid[posx,posy] = ((255,255,255),(title,classtxt,idtxt,text))
              else:
                  if self.disptext:
                      if val != 0:
                          text = str(int(val))
                      else:
                          text = ''
                  colorvalue = self.convert_to_rgb(minval, maxval, val, colors=[(0, 255, 0),(255,255,0),(255, 0, 0)])
                  self.grid[posx,posy] = (colorvalue,(title,classtxt,idtxt,text))
                    
#draw and save scalebar
      (FineColors, Colours) = self.colorBar(colors=[(0, 255, 0),(255,255,0),(255, 0, 0)],maxval=maxval)
    
#display bitmap and scalebar in notebook    
      if self.noshow == False:
          ScaleBar_html=self.html_table(d=Colours,maxval=maxval)
          self.ScaleBar_display(ScaleBar_html=ScaleBar_html)
          self.grid.show()

      if self.saveimage != None:
          self.grid.save_image(self.saveimage)
          self.ScaleBar_save(FineColors=FineColors, block_size=50, aspect_ratio=0.05, maxval=maxval)
            
#      print(self.rgb_to_hex((0, 255, 0)))


import sys
sys.path.insert(0, "D:\PyWeb\Pylib")

import seaborn as sns
import math
import matplotlib.pyplot as plt
import matplotlib.colors
import numpy as np
import re
import bokeh
import pandas as pd

#pd.set_option('display.max_columns', 80)
#pd.set_option('display.width', 200)

#DEVICEis='RAVEN1'
#df = pd.read_csv("merged.csv")
df_final = pd.read_csv("ReadyData.csv")
FailClassList = ['Double Bit BL']
#FailClassList = ['Unclassified','Single Bit','Double Bit BL','Double Bit WL','QuadBits','2x2Cluster','Single BL','Single WL','PartialSingle BL','PartialSingle WL','Double WL','PartialDouble WL','Four WL','PartialFour WL']

for i in FailClassList:
    print('\nFail Class is', i)
    WaferBitMap(df_DrawMap=df_final,plot=i,blocksize=45,saveimage=(i+'.png'))
